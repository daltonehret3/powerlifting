//
//  WorkoutBuilder.swift
//  powerlifting
//
//  Created by Dalton Ehret on 6/13/19.
//  Copyright © 2019 Dalton Ehret. All rights reserved.
//

import Foundation


class WorkoutDay1 {
    let weekNumber : Int
    let dayNumber : Int
    var workoutA1 : String
    var workoutB1 : String
    var workoutC1 : String
    var workoutC2 : String
    var workoutD1 : String
    var workoutD2 : String
    
    
    
    init(week: Int, day: Int){
        weekNumber = week
        dayNumber = day
        workoutA1 = ""
        workoutB1  = ""
        workoutC1 = ""
        workoutC2 = ""
        workoutD1 = ""
        workoutD2 = ""
        SetWorkouts()
    }

    
    func SetWorkouts(){
        if(dayNumber == 1){
            workoutA1 = "Squat"
            workoutB1 = "Bench"
            workoutC1 = "SB Body Saw"
            workoutC2 = "Weighted Glute Bridge"
            workoutD1 = "Hip Circle Side Steps"
            workoutD2 = "Face Pulls"
        }else if(dayNumber == 2){
            workoutA1 = "3ct Pause Squat"
            workoutB1 = "3ct Pause Bench"
            workoutC1 = "1 Arm Dead Stop Row"
            workoutC2 = "DB Floor Press"
            workoutD1 = "DB Hammer Curl"
            workoutD1 = "Tricep Pushdowns"
        }else if(dayNumber == 3){
            workoutA1 = "Deadlift"
            workoutB1 = "Deficit Deadlift"
            workoutC1 = "3ct Wide Grip Feet Up Bench"
            workoutC2 = "KB Goblet Squat"
            workoutD1 = "Hanging Leg Raise"
            workoutD2 = "Pull Ups or Lat Pulldowns"
        }else if(dayNumber == 4){
            workoutA1 = "Incline Bench"
            workoutB1 = "Overhead Press"
            workoutC1 = "3ct DB Bench"
            workoutC2 = "Machine Rows"
            workoutD1 = "Tricep Push Down"
            workoutD2 = "Straight Bar Curl"
        }
    }
    
}
