//
//  DayViewController.swift
//  powerlifting
//
//  Created by Dalton Ehret on 6/13/19.
//  Copyright © 2019 Dalton Ehret. All rights reserved.
//

import UIKit

class DayViewController : UICollectionViewController{
    

    var name: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = name
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    // Populate View
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! myCell
        
        cell.textLbl.text = "SEGUE WORKED"
        return cell
    }
}
