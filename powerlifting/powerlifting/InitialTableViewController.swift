//
//  InitialTableViewController.swift
//  powerlifting
//
//  Created by Dalton Ehret on 6/13/19.
//  Copyright © 2019 Dalton Ehret. All rights reserved.
//

import UIKit


class InitialTableViewController : UITableViewController{
    var numberOfWeeks = 4
    var myIndex = 0
    var text = ""
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let number = indexPath.row + 1
        text = "Week \(number)"
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = UIFont.systemFont(ofSize: 20.0)
        cell.textLabel?.text = text

        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myIndex = indexPath.row
        performSegue(withIdentifier: "weekToDay", sender: myIndex)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "weekToDay" {
            let destinationVC = segue.destination as! DayViewController

            destinationVC.name = "Week \(myIndex+1)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
